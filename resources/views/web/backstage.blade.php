<!DOCTYPE html>
<html lang="z-hant">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}" />
  <title>集寵 官網後台管理</title>
  <link rel="icon" type="image/png" href="/images/OnePage/logo.png">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/backstage.css?ver=2019080801" rel="stylesheet">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.2/css/all.css" rel="stylesheet">
</head>


<body>
  @include('web.notifications.send-success')
    @include('web.backstage-nav')
    <main>
      <div class="container px-4 px-sm-5">
        <h3 class="font-weight-bold mb-3">訪客來信 /</h3>
        @if(Count($feedbacks) > 0)
        <div class="mb-3 d-flex align-items-center d-lg-none">
          <input name="selectAll_chk" type="checkbox" class="thCk mr-2">
          全選
          <i name="trash_can" class="far mb-1"></i>
        </div>
        @endif
        <table class="table border text-center">
          <thead class="thead-light">
            <tr>
              <th scope="col">
              @if(Count($feedbacks) > 0)
              <input name="selectAll_chk" type="checkbox" class="thCk">
              <i name="trash_can" class="far "></i>
              @endif
            </th>
            <th scope="col">留言日期</th>
            <th scope="col">會員姓名</th>
            <th scope="col">連絡方式</th>
            <th scope="col"></th>
          </tr>
        </thead>
        {{Form::open(['route' => 'detail.delete' , 'method' => 'delete' , 'id' => 'delete_form'])}}
        <tbody>
          @foreach($feedbacks as $index => $feedback)
          <tr>
            <th scope="row"><input name="opinion_chk[]" type="checkbox" value="{{$feedback->id}}"></th>
            <td data-label="留言日期">
              <span class="d-inline-block">
                {{$feedback->created_at->format("Y/m/d")}}<span
                  class="d-inline-block d-lg-block text-left pl-3 pl-lg-0">{{$feedback->created_at->format("H:i:s")}}</span>
              </span>
            </td>
            <td data-label="會員姓名">{{$feedback->name}}</td>
            <td data-label="連絡方式">{{$feedback->email}}</td>
            <td data-label="">
              <input type="button" name="read_btn"
                class="btn {{$feedback->is_read == 0 ? "btn-info" : "btn-warning"}} text-white font-weight-bold"
                data-toggle="modal" data-target="#exampleModalCenter" data-id={{encrypt($feedback->id)}}
                value="{{$feedback->is_read == 0 ? "未讀" : "已讀"}}">
            </td>
          </tr>
          @endforeach
        </tbody>
        {{Form::close()}}
      </table>
    </div>
    {{$feedbacks->links('pagination.default')}}

    <!-- 互動視窗 -->
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog"
      aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title d-flex flex-wrap" id="exampleModalLongTitle">
              <div class="row w-100">
                <div class="col-12 col-lg-5">姓名：<span class="c-blue" name="name"></span></div>
                <div class="col-12 col-lg-7 d-flex flex-wrap mt-2 mt-lg-0">
                  <div class="col-auto px-0">留言時間：</div>
                  <span class="col-auto px-0" name="create_time"></span>
                </div>
              </div>
              <div class="col-12 pl-0 mt-2">信箱：<span name="email"></span></div>
            </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body" name="content">

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">關閉</button>
          </div>
        </div>
      </div>
    </div>

  </main>

  <script src="/js/jquery-3.3.1.min.js"></script>
  <script src="/js/bootstrap.min.js"></script>
  <script src="/js/bootstrap.bundle.min.js"></script>
  <script>
    $("input[name='read_btn']").on('click', function () {
      let input = $(this);
      $.ajax({
        url: '/visitor-opinion/detail',
        type: 'GET',
        data: {
          id: $(this).data('id'),
        },
        error: function (xhr) {
          alert('發生錯誤');
        },
        success: function (response) {
          if (response.is_read == 0) {
            chgBtnStyle(input);
          }
          $("span[name='name']").html(response.name);
          $("span[name='email']").html(response.email);
          $("span[name='create_time']").html(response.create_time);
          $("div[name='content']").html(response.content);
        }
      });
    });

    function chgBtnStyle(input) {
      $(input).val("已讀");
      $(input).removeClass("btn-info");
      $(input).addClass("btn-warning");
    }

    $("input[name='selectAll_chk']").on('click', function () {
      if ($(this).prop('checked')) {
        $("input[name='opinion_chk[]']").prop('checked', true);
        $("i[name='trash_can']").addClass("fa-trash-alt");
      } else {
        $("input[name='opinion_chk[]']").prop('checked', false);
        $("i[name='trash_can']").removeClass("fa-trash-alt");
      }
    });

    $("input[name='opinion_chk[]']").on('click', function () {
      if ($("input[name='opinion_chk[]']:checked").length > 0) {
        $("i[name='trash_can']").addClass("fa-trash-alt");
      } else {
        $("i[name='trash_can']").removeClass("fa-trash-alt");
      }

      if ($("input[name='opinion_chk[]']:checked").length == $("input[name='opinion_chk[]']").length) {
        $("input[name='selectAll_chk']").prop("checked", true);
      } else {
        $("input[name='selectAll_chk']").prop("checked", false);
      }
    });

    $("i[name='trash_can']").on('click', function () {
      if (confirm('確定要刪除?')) {
        $("#delete_form").submit();
      }
    });
  </script>
</body>

</html>