@extends('web.web')

@section('content')
<section class="container-fluid" id="news-detail">
  <div class="container position-relative">
    <div class="row">
      <div class="col-12 col-md-8 d-flex flex-column pr-md-5 border-right border-secondary">
        <img src="{{$news->images[0]->image->url()}}">
        <h4>{{$news->title}}</h4>
        <small>{{$news->created_at->format("m月d.Y")}}</small>
        <textarea id="content_area" class="text-justify mb-3" readonly>{{$news->content}}</textarea>
      </div>
      <div class="col-12 col-md-4 pl-md-4">
        <h4 class="mb-3">近期文章</h4>
        @foreach($datas as $index => $data) 
          <a href="/news/{{$data->id}}/detail" class="title-R">{{$data->title}}&emsp;<span class="date-R">{{$data->created_at->format("m月d.Y")}}</span></a>
        @endforeach
      </div>
    </div>
  </div>
</section>
@endsection
@section('script')
<script>
  $(document).ready(function() {
    $("#content_area").height($("#content_area")[0].scrollHeight);
  });
</script>
@endsection