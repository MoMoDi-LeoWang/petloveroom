@extends('web.web')

@section('link')
<link href="/css/swiper.min.css" rel="stylesheet">
@endsection

@section('content')
<section class="container-fluid" id="about">
  <div class="text-center">
    <div class="w-100 videoBox mb-5">
      <video src="/images/OnePage/01.mp4" controls="controls" class="container video my-3"></video>
    </div>
    <h2 class="c-blue mb-4">舒適、專業、放心</h2>
    <h3 class="mb-4">我們的故事</h3>
    <p class="text-justiyf text-sm-center mb-4 mb-md-5 font-weight-bold aboutText">
      <span>用心栽培有愛心、耐心的美容師與保姆們，成為「集」一身「寵」愛給毛孩的強大團隊。也因為如此堅持初衷並不斷進修與創新，集寵經營了十多年，目前已有三間美容分店及一間住宿館。</span>
      <!-- <span class="d-block">最後真的很感謝大家一路相伴，也會盡所能做到最好</span> -->
    </p>
    <h3 class="mb-4">我們的堅持</h3>
    <p class="text-justiyf text-sm-center mb-4 mb-md-5 font-weight-bold aboutText">
      <span>好的產品固然重要，但倘若能兼具美容師的純熟技術、寵物的專業知識、與家長的良好溝通，才能讓毛孩們擁有高品質、高享受的殿堂級服務，快快樂樂的美容，漂漂亮亮的回家。</span>
    </p>
  </div>
  <div class="row text-center">
    <div class="col-6 col-md-3 mb-4 mb-md-0">
      <img src="/images/OnePage/img01.jpg" class="wow fadeInDown" data-wow-offset="100">
    </div>
    <div class="col-6 col-md-3 mb-4 mb-md-0">
      <img src="/images/OnePage/img02.jpg" class="wow fadeInDown" data-wow-offset="100" data-wow-delay="0.3s">
    </div>
    <div class="col-6 col-md-3" class="wow fadeInDown" data-wow-offset="100" data-wow-delay="0.5s">
      <img src="/images/OnePage/img03.jpg" class="wow fadeInDown" data-wow-offset="100" data-wow-delay="0.6s">
    </div>
    <div class="col-6 col-md-3">
      <img src="/images/OnePage/img04.jpg" class="wow fadeInDown" data-wow-offset="100" data-wow-delay="0.7s">
    </div>
  </div>
</section>
<section class="container-fluid position-relative" id="aboutContent">
  <div class="outsideCircle"></div>
  <div class="circle"></div>
  <div class="innerCircle"></div>
  <div class="row bg-blue">
    <div class="aboutBox">
      <div class="col-12 col-sm-6 col-md-6 d-flex align-items-center txtL">
        <div class="position-relative">
          <div class="itemsBorder wow" data-wow-offset="150"></div>
          <p class="aboutTitle wow" data-wow-offset="150" data-wow-delay="0.8s">專業執照</p>
          <ul class="aboutItems wow" data-wow-offset="100" data-wow-delay="1s">
            <li>動保處特許核准寵物寄養業者</li>
            <li>都發局特許核准寵物寄養業者</li>
            <li>特寵業V1080242-00號</li>
          </ul>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-md-6 order-1 order-sm-0 imgR">
        <div class="swiper-container wow fadeInLeft" data-wow-offset="150">
          <div class="swiper-wrapper">
            <div class="swiper-slide">
              <img src="/images/OnePage/aboutImg02.png" class="aboutImg">
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="aboutBox">
      <div class="col-12 col-sm-6 col-md-6 d-center order-1 order-sm-0 imgL">
        <div class="swiper-container wow fadeInLeft" data-wow-offset="150">
          <div class="swiper-wrapper">
            <div class="swiper-slide">
              <img src="/images/OnePage/aboutImg03.png" class="aboutImg">
            </div>
            <div class="swiper-slide">
              <img src="/images/OnePage/aboutImg04.png" class="aboutImg">
            </div>
          </div>
          <!-- Add Pagination -->
          <div class="swiper-pagination swiper-pagination-white"></div>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-md-6 d-flex align-items-center txtR">
        <div class="position-relative">
          <div class="itemsBorder wow" data-wow-offset="150"></div>
          <p class="aboutTitle wow" data-wow-offset="150" data-wow-delay="0.8s">專業照護</p>
          <ul class="aboutItems wow" data-wow-offset="100" data-wow-delay="1s">
            <li>文旅專屬獸醫師健診</li>
            <li>頂級寵物美容服務</li>
          </ul>
        </div>
      </div>
    </div>
    <div class="aboutBox">
      <div class="col-12 col-sm-6 col-md-6 d-flex align-items-center txtL">
        <div class="position-relative">
          <div class="itemsBorder wow" data-wow-offset="150"></div>
          <p class="aboutTitle wow" data-wow-offset="150" data-wow-delay="0.8s">專人陪伴</p>
          <ul class="aboutItems wow" data-wow-offset="100" data-wow-delay="1s">
            <li>24小時專業褓母陪伴照護</li>
            <li>一對一遊戲及多元化嗅聞遊戲課程</li>
          </ul>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-md-6 imgR">
        <div class="swiper-container wow fadeInLeft" data-wow-offset="150">
          <div class="swiper-wrapper">
            <div class="swiper-slide">
              <img src="/images/OnePage/aboutImg05.png" class="aboutImg">
            </div>
            <div class="swiper-slide">
              <img src="/images/OnePage/aboutImg06.png" class="aboutImg">
            </div>
          </div>
          <!-- Add Pagination -->
          <div class="swiper-pagination swiper-pagination-white"></div>
        </div>
      </div>
    </div>
    <div class="aboutBox">
      <div class="col-12 col-sm-6 col-md-6 d-center order-1 order-sm-0 imgL">
        <div class="swiper-container wow fadeInLeft" data-wow-offset="150">
          <div class="swiper-wrapper">
            <div class="swiper-slide">
              <img src="/images/OnePage/aboutImg07.png" class="aboutImg">
            </div>
            <div class="swiper-slide">
              <img src="/images/OnePage/aboutImg08.png" class="aboutImg">
            </div>
          </div>
          <!-- Add Pagination -->
          <div class="swiper-pagination swiper-pagination-white"></div>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-md-6 d-flex align-items-center txtR">
        <div class="position-relative">
          <div class="itemsBorder wow" data-wow-offset="150"></div>
          <p class="aboutTitle wow" data-wow-offset="150" data-wow-delay="0.8s">專業設備</p>
          <ul class="aboutItems wow" data-wow-offset="100" data-wow-delay="1s">
            <li>全屋無毒無甲醛綠建材設備</li>
            <li>大金空調全熱型交換器，空氣清新</li>
            <li>動物安全地毯設施、止滑柔軟，具醫療級標準</li>
          </ul>
        </div>
      </div>
    </div>
    <div class="aboutBox">
      <div class="col-12 col-sm-6 col-md-6 d-flex align-items-center txtL">
        <div class="position-relative">
          <div class="itemsBorder wow" data-wow-offset="150"></div>
          <p class="aboutTitle wow" data-wow-offset="150" data-wow-delay="0.8s">專屬空間</p>
          <ul class="aboutItems wow" data-wow-offset="100" data-wow-delay="1s">
            <li>國際綠建築認證無毒精油安撫空地</li>
            <li>汪星人、喵星人家庭式住屋</li>
            <li>日間自由於空地、夜間專屬休息室</li>
          </ul>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-md-6 imgR">
        <div class="swiper-container wow fadeInLeft" data-wow-offset="150">
          <div class="swiper-wrapper">
            <div class="swiper-slide">
              <img src="/images/OnePage/aboutImg09.png" class="aboutImg">
            </div>
            <div class="swiper-slide">
              <img src="/images/OnePage/aboutImg10.jpg" class="aboutImg">
            </div>
          </div>
          <!-- Add Pagination -->
          <div class="swiper-pagination swiper-pagination-white"></div>
        </div>
      </div>
    </div>
    <div class="aboutBox">
      <div class="col-12 col-sm-6 col-md-6 d-center order-1 order-sm-0 imgL">
        <div class="swiper-container wow fadeInLeft" data-wow-offset="150">
          <div class="swiper-wrapper">
            <div class="swiper-slide">
              <img src="/images/OnePage/aboutImg11.png" class="aboutImg">
            </div>
            <div class="swiper-slide">
              <img src="/images/OnePage/aboutImg12.png" class="aboutImg">
            </div>
          </div>
          <!-- Add Pagination -->
          <div class="swiper-pagination swiper-pagination-white"></div>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-md-6 d-flex align-items-center txtR">
        <div class="position-relative">
          <div class="itemsBorder wow" data-wow-offset="150"></div>
          <p class="aboutTitle wow" data-wow-offset="150" data-wow-delay="0.8s">營養鮮食</p>
          <ul class="aboutItems wow" data-wow-offset="100" data-wow-delay="1s">
            <li>無調味純天然新鮮食材</li>
            <li>排骨燉製湯底、營養均衡不挑食</li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <!-- <img src="/images/OnePage/land.png" class="greenland" alt=""> -->

</section>

@endsection

@section('script')
<script src="/js/swiper.min.js"></script>
<script>
  var swiper = new Swiper('.swiper-container', {
    spaceBetween: 30,
    effect: 'fade',
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    },
    autoplay: {
      delay: 5000,
    },
  });
</script>
@endsection