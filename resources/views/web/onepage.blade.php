<!DOCTYPE html>
<html lang="z-hant">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>集寵洗澡間</title>
  <link rel="icon" type="image/png" href="/images/OnePage/logo.png">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/animate.min.css" rel="stylesheet">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.2/css/all.css" rel="stylesheet">
  <link href="/css/onepage.css" rel="stylesheet">
</head>

<body data-spy="scroll" data-target="#navbar-example">
  @include('web.notifications.send-success')
  <div id="content" class="slideout-panel slideout-panel-right">
    <nav class="navbar navbar-expand-xl navbar-light fixed-top" id="navbar">
      <div class="container">
        <a class="navbar-brand pb-0 pt-0" href="#">
          <img src="/images/OnePage/logo.png" class="logo">
          <span class="logoTitle">集寵洗澡間</span>
        </a>
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" href="#home">
                首頁<span class="line"></span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#about">
                關於我們<span class="line"></span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#serviceItems">
                服務項目<span class="line"></span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#news">
                最新消息<span class="line"></span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#contactInfo">
                聯絡我們<span class="line"></span>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <main class="mt-0">
      <section class="container-fluid fadeIn" id="home">
        <div class="position-relative innH">
          <h1 class="text-center position-absolute title">集寵洗澡間<br>呵護你的毛孩子</h1>
          <a href="#about" class="downBt">
            <i class="fas fa-chevron-circle-down downIcon position-absolute"></i>
          </a>
        </div>
      </section>
      <section class="container-fluid" id="about">
        <div class="text-center">
          <h2 class="c-brown">舒適、專業、放心</h2>
          <h4 class="c-blue mb-4">我們的故事</h4>
          <p class="text-left text-sm-center mb-4 mb-md-5">
            <span>集寵的信念，一直都是希望能夠給予毛孩最舒適的環境、讓毛爸毛媽能夠安心的託付於我們！</span>
            <span class="d-block">最後真的很感謝大家一路相伴，也會盡所能做到最好</span>
          </p>
          <h4 class="c-blue mb-4">我們的堅持</h4>
          <p class="text-left text-sm-center mb-4 mb-md-5">
            好的產品固然重要，但倘若能兼具美容師的純熟手法與技術、寵物的專業知識、與家長的良好溝通，才能讓毛孩們擁有高品質、高享受的殿堂級服務，快快樂樂的美容、漂漂亮亮的回家。
          </p>
        </div>
        <div class="row text-center">
          <div class="col-6 col-md-3 mb-4 mb-md-0">
            <img src="/images/OnePage/img01.jpg" class="wow fadeInDown" data-wow-offset="150">
          </div>
          <div class="col-6 col-md-3 mb-4 mb-md-0">
            <img src="/images/OnePage/img02.jpg" class="wow fadeInDown" data-wow-offset="150" data-wow-delay="0.3s">
          </div>
          <div class="col-6 col-md-3" class="wow fadeInDown" data-wow-offset="150" data-wow-delay="0.5s">
            <img src="/images/OnePage/img03.jpg" class="wow fadeInDown" data-wow-offset="150" data-wow-delay="0.6s">
          </div>
          <div class="col-6 col-md-3">
            <img src="/images/OnePage/img04.jpg" class="wow fadeInDown" data-wow-offset="150" data-wow-delay="0.7s">
          </div>
        </div>
      </section>
      <section class="container-fluid position-relative" id="serviceItems">
        <img src="/images/OnePage/cloud-B.svg" class="position-absolute cloud-B1" alt="">
        <img src="/images/OnePage/cloud-B.svg" class="position-absolute cloud-B2" alt="">
        <div class="container">
          <h2 class="c-brown text-center">服務項目</h2>
          <div class="row">
            <div class="col-12 d-flex justify-content-sm-center text-center mb-4 mb-sm-5 items">
              <div class="col-auto col-lg-3 h2">洗澡</div>
              <div class="col-auto col-lg-3 h2">造型美容</div>
              <div class="col-auto col-lg-3 h2">藥浴SPA</div>
              <div class="col-auto col-lg-3 h2">住宿</div>
            </div>
            <div
              class="col-12 col-md-8 col-lg-9 d-flex flex-column justify-content-center font-weight-bold pl-3 pl-md-5">
              <p class="w-100 text-justify">
                小美容服務項目:
                洗澡+剪指甲+剃腳底毛+剃肚子毛+清除耳毛+剃肛門毛+擠肛門線+眼部周圍雜毛修剪。</p>
              <p class="w-100 text-justify">
                大美容服務項目:
                洗澡+剪指甲+剃腳底毛+剃肚子毛+清除耳毛+剃肛門毛+擠肛門線+造型修剪。</p>
            </div>
            <div class="col-12 col-md-4 col-lg-3">
              <div class="circle cImg01 wow rollIn" data-wow-offset="150"></div>
            </div>
          </div>
        </div>
      </section>
      <section class="container-fluid" id="serviceContent">
        <div class="container">
          <div class="row">
            <div class="col-12 col-lg-9 d-flex flex-wrap align-items-center order-sm-1 order-lg-0">
              <div class="cloud position-relative" data-text="造型美容">
                <img src="/images/OnePage/cloud-W.svg" class="cloud-W">
              </div>
              <h4 class="serviceText">針對不同毛髮與皮膚量身打造專屬保養，深層清潔、水療按摩、促進新陳代謝。</h4>
              <hr>
            </div>
            <div class="col-12 col-sm-6 col-lg-3 mb-lg-5 order-sm-3 order-lg-0">
              <div class="circle cImg02 wow rollIn" data-wow-offset="150"></div>
            </div>
            <div class="col-12 col-lg-9 d-flex flex-wrap align-items-center order-sm-2 order-lg-0 mb-4">
              <div class="cloud position-relative" data-text="藥浴SAP">
                <img src="/images/OnePage/cloud-W.svg" class="cloud-W">
              </div>
              <h4 class="serviceText">(1)殺菌藥浴、(2)殺黴菌藥浴、(3)除蚤藥浴三種。一般狗貓皆可使用，可當預防保健。另外有洗皮脂漏藥浴，為專治皮脂漏皮膚病。</h4>
            </div>
            <div class="col-12 col-sm-6 col-lg-3 order-sm-3 order-lg-0">
              <div class="circle cImg03 wow rollIn" data-wow-offset="150"></div>
            </div>
          </div>
        </div>
      </section>
      <section class="container-fluid" id="news">
        <div class="container position-relative">
          <h2 class="c-brown text-center">最新消息</h2>
          <div class="row" data-wow-offset="200">
            <div class="col-6 col-sm-6 col-md-4 col-lg-3 d-flex flex-column wow fadeInLeft" data-wow-delay="">
              <img src="/images/OnePage/img07.jpg">
              <p class="h4">最新促銷活動</p>
              <small>八月15.2019</small>
              <span class="text-justify mb-3">除了各家寵物美容師知識交流醫學常識 也是集寵姊姊們努力學習的方向不斷精進與多元化學習的動力</span>
            </div>
            <div class="col-6 col-sm-6 col-md-4 col-lg-3 d-flex flex-column wow fadeInLeft" data-wow-delay="0.3s">
              <img src="/images/OnePage/img07.jpg">
              <p class="h4">最新促銷活動</p>
              <small>八月15.2019</small>
              <span class="text-justify mb-3">除了各家寵物美容師知識交流醫學常識 也是集寵姊姊們努力學習的方向不斷精進與多元化學習的動力</span>
            </div>
            <div class="col-6 col-sm-6 col-md-4 col-lg-3 d-flex flex-column wow fadeInLeft" data-wow-delay="0.5s">
              <img src="/images/OnePage/img07.jpg">
              <p class="h4">最新促銷活動</p>
              <small>八月15.2019</small>
              <span class="text-justify mb-3">除了各家寵物美容師知識交流醫學常識 也是集寵姊姊們努力學習的方向不斷精進與多元化學習的動力</span>
            </div>
            <div class="col-6 col-sm-6 col-md-4 col-lg-3 d-flex flex-column wow fadeInLeft" data-wow-delay="0.7s">
              <img src="/images/OnePage/img07.jpg">
              <p class="h4">最新促銷活動</p>
              <small>八月15.2019</small>
              <span class="text-justify mb-3">除了各家寵物美容師知識交流醫學常識 也是集寵姊姊們努力學習的方向不斷精進與多元化學習的動力</span>
            </div>
            <div class="col-6 col-sm-6 col-md-4 col-lg-3 d-flex flex-column wow fadeInLeft" data-wow-delay="0.9s">
              <img src="/images/OnePage/img07.jpg">
              <p class="h4">最新促銷活動</p>
              <small>八月15.2019</small>
              <span class="text-justify mb-3">除了各家寵物美容師知識交流醫學常識 也是集寵姊姊們努力學習的方向不斷精進與多元化學習的動力</span>
            </div>
            <div class="col-6 col-sm-6 col-md-4 col-lg-3 d-flex flex-column wow fadeInLeft" data-wow-delay="1.1s">
              <img src="/images/OnePage/img07.jpg">
              <p class="h4">最新促銷活動</p>
              <small>八月15.2019</small>
              <span class="text-justify mb-3">除了各家寵物美容師知識交流醫學常識 也是集寵姊姊們努力學習的方向不斷精進與多元化學習的動力</span>
            </div>

          </div>
        </div>
      </section>
      <section class="container-fluid" id="contactInfo">
        <div class="container">
          <h2 class="c-purple text-center">聯絡資訊</h2>
          <div class="row">
            <div class="col-12 col-sm-6 col-md-4 col-xl-3 d-flex flex-column">
              <img src="/images/OnePage/img09.jpg" class="wow zoomIn" data-wow-offset="150">
              <h3 class="c-purple">成功店</h3>
              <small>台北市文山區木柵路2段29號</small>
              <span>02-2939-3757</span>
              <div class="d-flex">
                <a href="#"><img src="/images/OnePage/fb.png" class="Icon mb-xl-0"></a>
                <a href="#" class="ig"><img src="/images/OnePage/ig.png" class="Icon mb-xl-0"></a>
              </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-xl-3 d-flex flex-column">
              <img src="/images/OnePage/img09.jpg" class="wow zoomIn" data-wow-offset="150" data-wow-delay="0.3s">
              <h3 class="c-purple">成功店</h3>
              <small>台北市文山區木柵路2段29號</small>
              <span>02-2939-3757</span>
              <div class="d-flex">
                <a href="#"><img src="/images/OnePage/fb.png" class="Icon mb-xl-0"></a>
                <a href="#" class="ig"><img src="/images/OnePage/ig.png" class="Icon mb-xl-0"></a>
              </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-xl-3 d-flex flex-column">
              <img src="/images/OnePage/img09.jpg" class="wow zoomIn" data-wow-offset="150" data-wow-delay="0.5s">
              <h3 class="c-purple">成功店</h3>
              <small>台北市文山區木柵路2段29號</small>
              <span>02-2939-3757</span>
              <div class="d-flex">
                <a href="#"><img src="/images/OnePage/fb.png" class="Icon mb-xl-0"></a>
                <a href="#" class="ig"><img src="/images/OnePage/ig.png" class="Icon mb-xl-0"></a>
              </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-xl-3 d-flex flex-column">
              <img src="/images/OnePage/img09.jpg" class="wow zoomIn" data-wow-offset="150" data-wow-delay="0.7s">
              <h3 class="c-purple">成功店</h3>
              <small>台北市文山區木柵路2段29號</small>
              <span>02-2939-3757</span>
              <div class="d-flex">
                <a href="#"><img src="/images/OnePage/fb.png" class="Icon mb-0"></a>
                <a href="#" class="ig"><img src="/images/OnePage/ig.png" class="Icon mb-0"></a>
              </div>
            </div>

          </div>
        </div>
      </section>
      <section class="container-fluid" id="map">
        <div class="container">
          <div class="row">
            <div class="col-12 col-sm-6">
              <h2 class="c-purple">我們的位置</h2>
              <iframe
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3616.3312491027937!2d121.55743331460499!3d24.988857846254454!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3442aa0b3452e32b%3A0xe4302a6091e1d9a9!2zMTE25Y-w5YyX5biC5paH5bGx5Y2A5pyo5p-16Lev5LqM5q61MjnomZ8!5e0!3m2!1szh-TW!2stw!4v1565671909569!5m2!1szh-TW!2stw"
                height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
            <div class="col-12 col-sm-6 mt-4 mt-sm-0">
              {{Form::open(['route' => 'feedback.post','method' => 'post'])}}
              <h2 class="c-purple">聯絡我們</h2>
              <label>姓名*</label>
              <input type="text" name="name" class="form-control" required>
              <label>E-mail*</label>
              <input type="email" name="email" class="form-control" required>
              <label>建議及留言*</label>
              <textarea class="form-control" name="content" rows="3" required></textarea>
              <div class="w-100 text-right">
                <button type="submit" class="btn btn-secondary ml-auto">送出</button>
              </div>
              {{Form::close()}}
            </div>
          </div>
        </div>
      </section>
    </main>
    <footer class="pb-xl-0 text-center">
      <small>© Copyright 2019 - momodi Inc. | 網頁設計</small>
      <div class="d-flex d-xl-none justify-content-around footer">
        <a href="#home" class="footerItems">首頁</a>
        <a href="#about" class="footerItems">關於我們</a>
        <a href="#serviceItems" class="footerItems">服務項目</a>
        <a href="#news" class="footerItems">最新消息</a>
        <a href="#contactInfo" class="footerItems">聯絡我們</a>
      </div>
    </footer>
  </div>

  <script src="/js/jquery-3.3.1.min.js"></script>
  <script src="/js/bootstrap.min.js"></script>
  <script src="/js/wow.min.js"></script>
  <script src="/js/onepage.js"></script>
</body>

</html>