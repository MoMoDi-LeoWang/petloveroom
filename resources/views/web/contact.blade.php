@extends('web.web')

<style>
  main {
    background-color: #FFFFEF;
  }
</style>

@section('content')
<section class="container-fluid" id="contactInfo">
  <div class="container py-xl-5">
    <h2 class="c-purple text-center">聯絡資訊</h2>
    <div class="row">
      <div class="col-12 col-sm-6 col-md-4 col-xl-3 d-flex flex-column px-4">
        <img src="/images/OnePage/contact01.png" class="wow zoomIn" data-wow-offset="150">
        <h3 class="c-purple">集寵洗澡間 成功店</h3>
        <h6>永和區成功路二段117號</h6>
        <h6>02-3233-4247</h6>
        <div class="d-flex">
          <a href="https://www.facebook.com/%E9%9B%86%E5%AF%B5%E6%B4%97%E6%BE%A1%E9%96%93-332057817252053/" target="_blank"><img src="/images/OnePage/fb.png" class="Icon mb-xl-0"></a>
          <a href="https://www.instagram.com/petrooooom2018/" class="ig" target="_blank"><img src="/images/OnePage/ig.png" class="Icon mb-xl-0"></a>
          <a href="https://line.me/ti/p/%40set9375i" class="" target="_blank"><img src="/images/OnePage/lineIcon.png" class="Icon mb-xl-0"></a>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-md-4 col-xl-3 d-flex flex-column px-4">
        <img src="/images/OnePage/contact02.png" class="wow zoomIn" data-wow-offset="150" data-wow-delay="0.3s">
        <h3 class="c-purple">集寵洗澡間 木柵店</h3>
        <h6>台北市文山區木柵路2段29號</h6>
        <h6>02-2939-3757</h6>
        <div class="d-flex">
          <a href="https://www.facebook.com/%E9%9B%86%E5%AF%B5%E6%B4%97%E6%BE%A1%E9%96%93-332057817252053/" target="_blank"><img src="/images/OnePage/fb.png" class="Icon mb-xl-0"></a>
          <a href="https://www.instagram.com/petrooooom2018/" class="ig" target="_blank"><img src="/images/OnePage/ig.png" class="Icon mb-xl-0"></a>
          <a href="https://line.me/ti/p/%40set9375i" class="" target="_blank"><img src="/images/OnePage/lineIcon.png" class="Icon mb-xl-0"></a>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-md-4 col-xl-3 d-flex flex-column px-4">
        <img src="/images/OnePage/contact03.png" class="wow zoomIn" data-wow-offset="150" data-wow-delay="0.5s">
        <h3 class="c-purple">集寵洗澡間 景美店</h3>
        <h6>台北市文山區景興路274號</h6>
        <h6>02-2933-2636</h6>
        <div class="d-flex">
          <a href="https://www.facebook.com/%E9%9B%86%E5%AF%B5%E6%B4%97%E6%BE%A1%E9%96%93-332057817252053/" target="_blank"><img src="/images/OnePage/fb.png" class="Icon mb-xl-0"></a>
          <a href="https://www.instagram.com/petrooooom2018/" class="ig" target="_blank"><img src="/images/OnePage/ig.png" class="Icon mb-xl-0"></a>
          <a href="https://line.me/ti/p/%40iqj3649z" class="" target="_blank"><img src="/images/OnePage/lineIcon.png" class="Icon mb-xl-0"></a>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-md-4 col-xl-3 d-flex flex-column px-4">
        <img src="/images/OnePage/contact04.png" class="wow zoomIn" data-wow-offset="150" data-wow-delay="0.7s">
        <h3 class="c-purple">集寵文旅‧睡覺間</h3>
        <h6>台北市文山區木柵路二段27號</h6>
        <h6>02-2939-8586</h6>
        <div class="d-flex">
          <a href="https://www.facebook.com/%E9%9B%86%E5%AF%B5%E6%96%87%E6%97%85%E7%9D%A1%E8%A6%BA%E9%96%93-2256449397802473/" target="_blank"><img src="/images/OnePage/fb.png" class="Icon mb-0"></a>
          <a href="https://www.instagram.com/petlovehotel0630/" class="ig" target="_blank"><img src="/images/OnePage/ig.png" class="Icon mb-0"></a>
          <a href="http://nav.cx/9s6HcEl" class="" target="_blank"><img src="/images/OnePage/lineIcon.png" class="Icon mb-xl-0"></a>
        </div>
      </div>

    </div>
  </div>
</section>
<section class="container-fluid" id="map">
  <div class="container py-sm-5">
    <div class="row">
      <div class="col-12 col-sm-6">
        <h2 class="c-purple">我們的位置</h2>
        <iframe
          src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3616.3312491027937!2d121.55743331460499!3d24.988857846254454!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3442aa0b3452e32b%3A0xe4302a6091e1d9a9!2zMTE25Y-w5YyX5biC5paH5bGx5Y2A5pyo5p-16Lev5LqM5q61MjnomZ8!5e0!3m2!1szh-TW!2stw!4v1565671909569!5m2!1szh-TW!2stw"
          height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
      </div>
      <div class="col-12 col-sm-6 mt-4 mt-sm-0">
        {{Form::open(['route' => 'feedback.post','method' => 'post'])}}
        <h2 class="c-purple">聯絡我們</h2>
        <label>姓名*</label>
        <input type="text" name="name" class="form-control" required>
        <label>E-mail*</label>
        <input type="email" name="email" class="form-control" required>
        <label>建議及留言*</label>
        <textarea class="form-control" name="content" rows="3" required></textarea>
        <div class="w-100 text-right">
          <button type="submit" class="btn btn-secondary ml-auto">送出</button>
        </div>
        {{Form::close()}}
      </div>
    </div>
  </div>
</section>

@endsection