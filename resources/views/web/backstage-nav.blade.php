<nav class="navbar navbar-expand-xl navbar-light fixed-top font-weight-bold" id="navbar">
  <div class="container">
    <a class="navbar-brand pb-0 pt-0" href="#">
      <img src="/images/OnePage/logo.png" class="logo">
      <span class="logoTitle">集寵洗澡間</span>
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
      aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item">
          <a class="nav-link" href="/backstage/news">
            最新消息
          </a>
        </li>
        <li class="nav-item">
          <div class="nav-link text-white Slash">
            /
          </div>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/backstage/visitor-opinion">
            訪客來信
          </a>
        </li>
        <li class="nav-item">
          <div class="nav-link text-white Slash">
            /
          </div>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/backstage/logout">
            登出
          </a>
        </li>
      </ul>
    </div>
  </div>
</nav>