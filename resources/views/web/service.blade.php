@extends('web.web')

<style>
  main {
    background-color: #FFFFEF;
  }

  .contact-span{
    top: 0px;
    left: 0px;
    background-color: lightblue;
    color: #333;
    font-weight: bold;
    width: 120px;
    opacity: 0;
    z-index: -1;
    transition: 0.5s cubic-bezier(0,2, 1, 1) ;
  }

  .contact-span::before{
    content: "";
    position: absolute;
    width: 16px;
    height:16px;
    bottom: -7px;
    left: 20px;
    background-color: lightblue;
    transform: rotate(45deg);
  }

  a:hover > .contact-span{
    top:-50px;
    opacity: 1;
  }
</style>
@section('lightbox')
{{-- @include('web.lightbox.priceDog') --}}
@include('web.lightbox.priceDog-1')
@include('web.lightbox.priceDog-2')
@include('web.lightbox.priceDog-3')
@include('web.lightbox.priceCat')
@endsection
@section('content')
<section class="container position-relative" id="service">
  <div class="semicircle"></div>
  <div class="row">
    <div class="d-flex flex-wrap mb-5 w-100">
      <div class="col-12 col-lg-4 mt-5 mt-lg-0 wow zoomIn animated" data-wow-offset="100">
        <div class="serviceBox mb-3">
          <h2>安&nbsp;親</h2>
          <img src="/images/OnePage/serviceIcon01.png" class="serviceIcon" alt="">
          <ul>
            <li>小型犬<span>50公分內</span>$120/hr</li>
            <li>中小型犬<span class="pr-3">60公分</span>$140/hr</li>
            <li>中型犬<span>70公分</span>$160/hr</li>
            <li>大型犬<span>80公分</span>$200/hr</li>
            <li>貓咪<span>$120/hr</span></li>
          </ul>
        </div>
        <div class="d-flex justify-content-center pt-1">
          <a class="text-center link-bt position-relative" target="_blank" href="http://nav.cx/9s6HcEl">
            睡覺間
            <span class="position-absolute contact-span">聯絡我們</span>
          </a>
        </div>
      </div>
      <div class="col-12 col-lg-4 mt-5 mt-lg-0 wow zoomIn animated" data-wow-offset="100" data-wow-delay="0.3s">
        <div class="serviceBox">
          <h2>住&nbsp;宿</h2>
          <img src="/images/OnePage/serviceIcon02.png" class="serviceIcon" alt="">
          <ul>
            <li>小型犬<span>50公分以下</span><img class="serviceIcon-sm mt-1 priceDog-1" src="/images/OnePage/serviceIcon04.png" alt=""></li>
            <li>中小型犬<span class="pr-3">60公分以下</span><img class="serviceIcon-sm mt-1 priceDog-2" src="/images/OnePage/serviceIcon04.png" alt=""></li>
            <li>中型犬<span>70公分以下</span><img class="serviceIcon-sm mt-1 priceDog-3" src="/images/OnePage/serviceIcon04.png" alt=""></li>
            <li>貓咪<img class="serviceIcon-sm mt-1 priceCat" src="/images/OnePage/serviceIcon04.png" alt=""></li>
          </ul>
        </div>
        <div class="d-flex justify-content-center mt-5 pt-3">
          <a class="text-center link-bt position-relative" target="_blank" href="http://nav.cx/9s6HcEl">
            睡覺間
            <span class="position-absolute contact-span">聯絡我們</span>
          </a>
        </div>
      </div>
      <div class="col-12 col-lg-4 mt-5 mt-lg-0 wow zoomIn animated" data-wow-offset="100" data-wow-delay="0.5s">
        <div class="serviceBox mb-3">
          <h2>美&nbsp;容</h2>
          <img src="/images/OnePage/serviceIcon03.png" class="serviceIcon mb-4" alt="">
          <ul>
            <li class="pet pb-1 d-centerCol"><span class="c-lightBlue">汪 星 人</span>洗澡$550起<br>剪毛$1,000起</li>
            <li class="pet pt-1 d-centerCol"><span class="c-lightBlue">喵 星 人</span>洗澡$1050起<br>剪毛$1,800起</li>
          </ul>
        </div>
        <div class="d-flex justify-content-lg-between justify-content-around">
          <a class="text-center link-bt position-relative" target="_blank" href="https://line.me/ti/p/%40set9375i">
            木柵店
            <span class="position-absolute contact-span">聯絡我們</span>
          </a>
          <a class="text-center link-bt position-relative" target="_blank" href="https://line.me/ti/p/%40iqj3649z">
            景美店
            <span class="position-absolute contact-span">聯絡我們</span>
          </a>
          <a class="text-center link-bt position-relative" target="_blank" href="https://line.me/ti/p/%40set9375i">
            永和店
            <span class="position-absolute contact-span">聯絡我們</span>
          </a>
        </div>
      </div>
    </div>
    <p class="w-100 text-center itemsH1 mt-5 pt-5">毛孩爸媽最擔心的事<br>我們幫您解決</p>
    <div class="w-100 d-flex flex-wrap itemsSection">
      <div class="col-12 col-sm-6 col-lg-4 mt-4">
        <div class="itemsBox" data-label="怕 打 結">
          <span>怕 打 結</span>
          <p class="itemsTitle">Afloat醫美洗</p>
          <p class="itemsText">神經醯胺保濕鎖水、加入蠶絲蛋白，幫助形成皮膚保護屏障，使毛髮不糾結，散發自然光澤，走出門好風光。</p>
          <div class="d-flex justify-content-between">
            <img src="/images/OnePage/itemsImg01.png" class="itemsImg" alt="">
          </div>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-lg-4 mt-4">
        <div class="itemsBox" data-label="怕 癢 癢">
          <span>怕 癢 癢</span>
          <p class="itemsTitle">香草尼姆浴</p>
          <p class="itemsText">泡澡清潔皮膚，無臭味長時間持續，使毛色健康亮麗維持皮毛良好狀態，放鬆身心靈，舒服好心情。</p>
          <div class="d-flex justify-content-between">
            <img src="/images/OnePage/itemsImg02.png" class="itemsImg" alt="">
          </div>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-lg-4 mt-4">
        <div class="itemsBox" data-label="怕 掉 毛">
          <span>怕 掉 毛</span>
          <p class="itemsTitle">牛奶浴</p>
          <p class="itemsText">換毛季節，永遠除不完的廢毛。深層清潔毛孔，帶走鬆動的毛髮，也可幫助代謝老舊角質，家裡不再雪花片片。</p>
          <div class="d-flex justify-content-between">
            <img src="/images/OnePage/itemsImg03.png" class="itemsImg" alt="">
          </div>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-lg-4 mt-4">
        <div class="itemsBox" data-label="怕 疹 子">
          <span>怕 疹 子</span>
          <p class="itemsTitle">碳酸泉</p>
          <p class="itemsText mb-2">針對不同毛髮與皮膚量身打造專屬保養，深層清潔、水療按摩、促進新陳代謝。</p>
          <div class="d-flex justify-content-between">
            <img src="/images/OnePage/itemsImg04.png" class="itemsImg mt-2" alt="">
          </div>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-lg-4 mt-4">
        <div class="itemsBox" data-label="怕 蟲 蟲 咬">
          <span>怕 蟲 蟲 咬</span>
          <p class="itemsTitle">天然有機草本泥</p>
          <p class="itemsText">夏季蚊蟲繁殖季節，可使用天然草藥讓蟲蟲不喜歡靠近，延緩臭味產生，為健康奠定基礎，維持健康。</p>
          <div class="d-flex justify-content-between">
            <img src="/images/OnePage/itemsImg05.png" class="itemsImg" alt="">
          </div>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-lg-4 mt-4">
        <div class="itemsBox" data-label="怕 緊 張 與 年 老">
          <span>怕 緊 張 與 年 老</span>
          <p class="itemsTitle">奈米深層淨膚</p>
          <p class="itemsText mb-2">毛髮油膩之貓咪和老犬尤其適合，清潔毛髮過多油脂髒污、避免細菌殘留、減輕洗浴吹乾時間、不含乙醛合成香料、不需搓揉。</p>
          <div class="d-flex justify-content-between">
            <img src="/images/OnePage/itemsImg06.png" class="itemsImg" alt="">
          </div>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-lg-4 mt-4 mt-xl-0">
        <div class="itemsBox" data-label="怕 出 門">
          <span>怕 出 門</span>
          <p class="itemsTitle">住宿與安親</p>
          <p class="itemsText mb-2" style="min-height: 119px">出門工作遊玩，擔心毛孩一個人在家，孤單寂寞覺得冷，有２４小時專業保母陪伴，獸醫不定時巡房。</p>
          <div class="d-flex justify-content-between">
          <img src="/images/OnePage/itemsImg07.png" class="itemsImg" alt="">
        </div>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-lg-4 mt-4 mt-xl-0">
        <div class="itemsBox" data-label="怕 不 乖">
          <span>怕 不 乖</span>
          <p class="itemsTitle">訓練課程</p>
          <p class="itemsText">由專業保母持續依照練習、耐心、獎勵、引導等正向教學方式，來矯正寶貝行為問題，協助毛孩自然融入社會化生活，成為乖寶寶。</p>
          <div class="d-flex justify-content-between">
          <img src="/images/OnePage/itemsImg08.png" class="itemsImg" alt="">
        </div>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-lg-4 mt-4">
        <div class="itemsBox" data-label="怕 不 吃 飯 飯">
          <span>怕 不 吃 飯 飯</span>
          <p class="itemsTitle">手做營養鮮食</p>
          <p class="itemsText">由新鮮蔬菜、肉類、澱粉等營養比例做五行均衡調配，使腸胃有效吸收天然營養元素，讓毛寶貝攝取適當的養分，不再只是吃肉肉。</p>
          <div class="d-flex justify-content-between">
          <img src="/images/OnePage/itemsImg09.png" class="itemsImg" alt="">
        </div>
        </div>
      </div>

    </div>

    <img src="/images/OnePage/cat.png" class="catImg" alt="">
</section>

@endsection
@section('script')
<script>

  // $("img.priceDog").on('click',function(){
  //   $("#priceDog").modal('show');
  // });
  
  $("img.priceDog-1").on('click',function(){
    $("#priceDog-1").modal('show');
  });

  $("img.priceDog-2").on('click',function(){
    $("#priceDog-2").modal('show');
  });

  $("img.priceDog-3").on('click',function(){
    $("#priceDog-3").modal('show');
  });

  $("img.priceCat").on('click',function(){
    $("#priceCat").modal('show');
  })
</script>
@endsection
