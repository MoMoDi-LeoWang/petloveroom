<!DOCTYPE html>
<html lang="z-hant">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>集寵洗澡間</title>
  <link rel="icon" type="image/png" href="/images/OnePage/logo.png">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/animate.min.css" rel="stylesheet">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.2/css/all.css" rel="stylesheet">
  <link href="/css/slideout.css" rel="stylesheet">
  <link href="/css/index.css?ver=2019082906" rel="stylesheet">
</head>

<body>
  @include('web.notifications.send-success')
  <menu id="menu" class="slideout-menu slideout-menu-left p-0 m-0">
    <ul class="m-0 p-0">
      <li><a href="/">首頁</a></li>
      <li><a href="/about">關於我們</a></li>
      <li><a href="/service">服務項目</a></li>
      <li><a href="/news">活動優惠</a></li>
      <li><a href="/contact">聯絡我們</a></li>
    </ul>
  </menu>
  <div id="content" class="slideout-panel slideout-panel-right">
    <nav class="navbar navbar-expand-xl navbar-light fixed-top" id="navbar">
      <div class="container">
        <a class="navbar-brand pb-0 pt-0" href="#">
          <img src="/images/OnePage/logo.png" class="logo">
        </a>
        <i class="fas fa-bars"></i>
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" href="/">
                首頁<span class="line"></span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/about">
                關於我們<span class="line"></span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/service">
                服務項目<span class="line"></span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/news">
                活動優惠<span class="line"></span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/contact">
                聯絡我們<span class="line"></span>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <main class="mt-0">
      <section class="container-fluid fadeIn" id="home">
        <div class="position-relative innH">
          <h1 class="text-center position-absolute title animated flipInX">集寵洗澡間<br>呵護你的毛孩子</h1>
        </div>
      </section>
    </main>
    <footer class="pb-0 text-center">
      <small>© Copyright 2019 - momodi Inc. | 網頁設計</small>
    </footer>
  </div>
  <script src="/js/jquery-3.3.1.min.js"></script>
  <script src="/js/bootstrap.min.js"></script>
  <script src="/js/slideout.min.js"></script>
  <script src="/js/index.js"></script>
</body>

</html>