<style>
    .minh-64px{
        min-height: 64px;
    }

    @media(max-width: 420px) {
        .p-5 {
            padding: 0 !important;
        }

        .mh-150px {
            max-height: 150px;
        }

        .mh-64px{
            max-height: 64px;
        }
    }
</style>
<div class="modal fade" id="priceDog-3" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content p-5" style="background-color: #feffef;">
            <div class="d-flex col-12 mh-150px">
                <div class="p-2 col-6 bd-highlight d-flex align-items-center justify-content-center">
                    <img class="mw-100" src="/images/OnePage/pricedog-04.png" alt="">
                </div>
                <div class="d-flex align-items-center justify-content-center p-2 col-6 bd-highlight">
                    <p style="font-size:20px;" class="m-0 font-weight-bold text-center">中型犬<br>70公分以下</p>
                </div>
            </div>
            <div class="d-flex col-12 mh-64px"
                style="border-bottom: 2px dashed !important;background-color:#c1e2f1;">
                <div class="d-flex align-items-center justify-content-center col-6 bd-highlight minh-64px">
                    <p style="font-size:20px;" class="m-0 font-weight-bold text-center">原價</p>
                </div>
                <div class="d-flex align-items-center justify-content-center col-6 bd-highlight minh-64px">
                    <p style="font-size:20px;" class="m-0 font-weight-bold text-center">$1480</p>
                </div>
            </div>
            <div class="d-flex col-12 mh-64px" style="border-bottom: 2px dashed">
                <div class="d-flex align-items-center justify-content-center p-2 col-6 bd-highlight minh-64px">
                    <p style="font-size:20px;" class="m-0 font-weight-bold text-center">平日7折<br>
                        <span style="font-size:14px;color:#43abf4;">週一~週四</span>
                    </p>
                </div>
                <div class="d-flex align-items-center justify-content-center p-2 col-6 bd-highlight minh-64px">
                    <p style="font-size:20px;" class="m-0 font-weight-bold text-center">$1036</p>
                </div>
            </div>
            <div class="d-flex col-12 mh-64px" style="border-bottom: 2px dashed !important;background-color:#c1e2f1;">
                <div class="d-flex align-items-center justify-content-center p-2 col-6 bd-highlight minh-64px">
                    <p style="font-size:20px;" class="m-0 font-weight-bold text-center">假日8折<br>
                        <span style="font-size:14px;color:#43abf4;">週五~週日</span>
                    </p>
                </div>
                <div class="d-flex align-items-center justify-content-center p-2 col-6 bd-highlight minh-64px">
                    <p style="font-size:20px;" class="m-0 font-weight-bold text-center">$1184</p>
                </div>
            </div>
            <div class="d-flex col-12 mh-64px">
                <div class="d-flex align-items-center justify-content-center p-2 col-6 bd-highlight minh-64px">
                    <p style="font-size:14px;" class="m-0 font-weight-bold text-center">除夕前三天～初十<br>
                        <span style="font-size:14px;color:#43abf4;">農曆年</span>
                    </p>
                </div>
                <div class="d-flex align-items-center justify-content-center p-2 col-6 bd-highlight minh-64px">
                    <p style="font-size:20px;" class="m-0 font-weight-bold text-center">$1480</p>
                </div>
            </div>
        </div>
    </div>
</div>