<style>
    @media(max-width: 420px){
        .p-5{
            padding: 0 !important;
        }
        .mh-150px{
            max-height: 150px;
        }
    }
</style>
<div class="modal fade bd-example-modal-lg" id="priceDog" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content p-5" style="background-color: #feffef;">
            <div class="d-flex col-12 mh-150px">
                <div class="p-2 col-3 d-flex align-items-center justify-content-center">
                    <img class="mw-100" src="/images/OnePage/pricedog-04.png" alt="">
                </div>
                <div class="d-flex align-items-center justify-content-center p-2 col-3">
                    <p style="font-size:20px;" class="m-0 font-weight-bold text-center">小型犬<br>50公分以下</p>
                </div>
                <div class="d-flex align-items-center justify-content-center p-2 col-3">
                    <p style="font-size:20px;" class="m-0 font-weight-bold text-center">中小型犬<br>60公分以下</p>
                </div>
                <div class="d-flex align-items-center justify-content-center p-2 col-3">
                    <p style="font-size:20px;" class="m-0 font-weight-bold text-center">中型犬<br>70公分以下</p>
                </div>
            </div>
            <div class="d-flex col-12 pt-2 pb-2 mh-150px" style="border-bottom: 2px dashed !important;background-color:#c1e2f1;">
                <div class="d-flex align-items-center justify-content-center p-2 col-3">
                    <p style="font-size:20px;" class="m-0 font-weight-bold text-center">原價</p>
                </div>
                <div class="d-flex align-items-center justify-content-center p-2 col-3">
                    <p style="font-size:20px;" class="m-0 font-weight-bold text-center">$1280</p>
                </div>
                <div class="d-flex align-items-center justify-content-center p-2 col-3">
                    <p style="font-size:20px;" class="m-0 font-weight-bold text-center">$1380</p>
                </div>
                <div class="d-flex align-items-center justify-content-center p-2 col-3">
                    <p style="font-size:20px;" class="m-0 font-weight-bold text-center">$1480</p>
                </div>
            </div>
            <div class="d-flex col-12 mh-150px" style="border-bottom: 2px dashed !important;">
                <div class="d-flex align-items-center justify-content-center p-2 col-3">
                    <p style="font-size:20px;" class="m-0 font-weight-bold text-center">平日7折<br>
                        <span style="font-size:14px;color:#43abf4;">週一~週四</span>
                    </p>
                </div>
                <div class="d-flex align-items-center justify-content-center p-2 col-3">
                    <p style="font-size:20px;" class="m-0 font-weight-bold text-center">$896</p>
                </div>
                <div class="d-flex align-items-center justify-content-center p-2 col-3">
                    <p style="font-size:20px;" class="m-0 font-weight-bold text-center">$966</p>
                </div>
                <div class="d-flex align-items-center justify-content-center p-2 col-3">
                    <p style="font-size:20px;" class="m-0 font-weight-bold text-center">$1036</p>
                </div>
            </div>
            <div class="d-flex col-12 mh-150px" style="border-bottom: 2px dashed !important;background-color:#c1e2f1;">
                <div class="d-flex align-items-center justify-content-center p-2 col-3">
                    <p style="font-size:20px;" class="m-0 font-weight-bold text-center">假日8折<br>
                        <span style="font-size:14px;color:#43abf4;">週五~週日</span>
                    </p>
                </div>
                <div class="d-flex align-items-center justify-content-center p-2 col-3">
                    <p style="font-size:20px;" class="m-0 font-weight-bold text-center">$1024</p>
                </div>
                <div class="d-flex align-items-center justify-content-center p-2 col-3">
                    <p style="font-size:20px;" class="m-0 font-weight-bold text-center">$1104</p>
                </div>
                <div class="d-flex align-items-center justify-content-center p-2 col-3">
                    <p style="font-size:20px;" class="m-0 font-weight-bold text-center">$1184</p>
                </div>
            </div>
            <div class="d-flex col-12 mh-150px">
                <div class="d-flex align-items-center justify-content-center p-2 col-3">
                    <p style="font-size:20px;" class="m-0 font-weight-bold text-center">除夕～初十<br>
                        <span style="font-size:14px;color:#43abf4;">農曆年</span>
                    </p>
                </div>
                <div class="d-flex align-items-center justify-content-center p-2 col-3">
                    <p style="font-size:20px;" class="m-0 font-weight-bold text-center">$1280</p>
                </div>
                <div class="d-flex align-items-center justify-content-center p-2 col-3">
                    <p style="font-size:20px;" class="m-0 font-weight-bold text-center">$1380</p>
                </div>
                <div class="d-flex align-items-center justify-content-center p-2 col-3">
                    <p style="font-size:20px;" class="m-0 font-weight-bold text-center">$1480</p>
                </div>
            </div>
        </div>
    </div>
</div>