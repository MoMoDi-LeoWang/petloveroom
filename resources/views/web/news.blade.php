@extends('web.web')

<style>
  #content {
    background-color: #FFFFEF;
  }
</style>

@section('content')
<section class="container-fluid h-auto" id="news">
  <div class="container position-relative">
    <h2 class="c-brown text-center">活動優惠</h2>
    <div class="row" data-wow-offset="200">
      @foreach($datas as $index => $data)
        <div class="col-12 col-sm-6 col-md-4 col-lg-3 d-flex flex-column wow fadeInLeft" data-wow-delay="">
          <a href="/news/{{$data->id}}/detail" class="text-center"><img src="{{$data->images[0]->image->url('big')}}"></a>
          <a href="/news/{{$data->id}}/detail" class="h4">{{$data->title}}</a>
          <small>{{$data->created_at->format("m月d.Y")}}</small>
          <span class="text-justify mb-3 span-content">{{$data->content}}</span>
        </div>
      @endforeach
    </div>
  </div>
</section>
@endsection