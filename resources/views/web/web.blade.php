<!DOCTYPE html>
<html lang="z-hant">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>集寵洗澡間</title>
  <link rel="icon" type="image/png" href="/images/OnePage/logo.png">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/slideout.css?ver=202001031" rel="stylesheet">
  <link href="/css/otherpage.css?ver=202001031" rel="stylesheet">
  <link href="/css/animate.min.css" rel="stylesheet">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.2/css/all.css" rel="stylesheet">
  @yield('link')
</head>

@yield('style')

<body>
  @yield('lightbox')
  @include('web.notifications.send-success')
  <menu id="menu" class="slideout-menu slideout-menu-left p-0 m-0">
    <ul class="m-0 p-0">
      <li><a href="/">首頁</a></li>
      <li><a href="/about">關於我們</a></li>
      <li><a href="/service">服務項目</a></li>
      <li><a href="/news">活動優惠</a></li>
      <li><a href="/contact">聯絡我們</a></li>
    </ul>
  </menu>
  <div id="content" class="slideout-panel slideout-panel-right">
    <nav class="navbar navbar-expand-xl navbar-light fixed-top" id="navbar">
      <div class="container">
        <a class="navbar-brand pb-0 pt-0" href="/">
          <img src="/images/OnePage/logo.png" class="logo">
          <span class="logoTitle">集寵洗澡間</span>
        </a>
        <i class="fas fa-bars"></i>
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" href="/">
                首頁<span class="line"></span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/about">
                關於我們<span class="line"></span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/service">
                服務項目<span class="line"></span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/news">
                活動優惠<span class="line"></span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/contact">
                聯絡我們<span class="line"></span>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <main class="mt-0">
      @yield('content')
    </main>
    <footer class="text-white">
      <div class="container">
        <div class="row">
        <div class="col-12 col-md-6 border-left">
          <div class="d-flex align-items-center justify-content-center justify-content-xl-start">
            <small>集寵採用momodi數位化雲端管理系統<br>以及APP預約系統，歡迎下載使用</small>
            <a href="https://momodi.com.tw/" target="_blank"><img src="/images/OnePage/mmdIcon.png" class="mmdIcon" alt=""></a>
          </div>
        </div>
        <div class="col-12 col-md-6 small mt-2 text-center text-md-left">
          © Copyright 2019 - momodi Inc. | 網頁設計
        </div>

        </div>
      </div>
    </footer>
  </div>

  <script src="/js/jquery-3.3.1.min.js"></script>
  <script src="/js/bootstrap.min.js"></script>
  <script src="/js/slideout.min.js"></script>
  <script src="/js/wow.min.js"></script>
  <script src="/js/otherpage.js"></script>

  @yield('script')
</body>

</html>