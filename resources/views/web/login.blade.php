<!DOCTYPE html>
<html lang="z-hant">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>集寵 官網後台管理登入</title>
  <link rel="icon" type="image/png" href="/images/OnePage/logo.png">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/otherpage.css?ver=2019080801" rel="stylesheet">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.2/css/all.css" rel="stylesheet">
</head>

<style>
  body,
  html {
    background-color: #dec09e;
    position: relative;
    height: 100%;
    width: 100%;
  }

  main#login {
    width: 20%;
    display: block;
    position: absolute;
    left: 50%;
    top: 50%;
    transform: translate(-50%, -50%);
    padding: 0;
  }

  #login .logo {
    width: 180px;
  }

  .btn-primary {
    background-color: rgba(10, 20, 146) !important;
    border: 0;
  }

  @media screen and (min-width: 1px) and (max-width: 1199.98px) {

    main#login {
      width: 90%;
    }

  }
</style>

<body>
  <main id="login">
    <div class="text-center">
      <img src="/images/OnePage/logo.png" class="logo">
    </div>
    <h1 class="text-center">後台登入</h1>
    {{Form::open(["route" => "back.login" , "method" => "post"])}}
    <div class="form-group">
      <label for="exampleInputEmail1">帳號</label>
      <input name="account" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
        placeholder="Account" required>
    </div>
    <div class="form-group">
      <label for="exampleInputPassword1">密碼</label>
      <input name="password" type="password" class="form-control" id="exampleInputPassword1" placeholder="Password"
        required>
    </div>
    <button type="submit" class="btn btn-primary w-100">登入</button>
    {{Form::close()}}
  </main>

  <script src="/js/jquery-3.3.1.min.js"></script>
  <script src="/js/bootstrap.min.js"></script>
  <script>

  </script>
</body>

</html>