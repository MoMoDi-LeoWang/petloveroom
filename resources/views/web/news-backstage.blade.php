<!DOCTYPE html>
<html lang="z-hant">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>集寵 官網後台管理 最新消息</title>
  <link rel="icon" type="image/png" href="/images/OnePage/logo.png">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/backstage.css?ver=2019082601" rel="stylesheet">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.2/css/all.css" rel="stylesheet">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.4.1/cropper.css" rel="stylesheet">
</head>

<style>
  /* for color */
  .c-brown {
    color: #998675;
  }

  .c-blue {
    color: #19248A;
  }

  .c-purple {
    color: #2F37A2;
  }

  #newsBs form {
    display: none;
  }

  input[type=file] {
    width: 100%;
    height: 100%;
    opacity: 0;
    cursor: pointer;
    position: absolute;
    top: 0;
  }

  .crop-rotate{
    cursor: pointer;
    width: 50px;
  }
</style>

<body>
  <div id="newsBs">
    @include('web.backstage-nav')
    <main>
      <div class="container px-4 px-xl-5">
        <div class="row">
          <div class="col-12 d-flex mb-3 align-items-center">
            <h3 class="font-weight-bold m-0">最新消息 /</h3>
            <button type="button" class="btn btn-brown ml-3" value="關閉" name="add">新 增</button>
          </div>
          {{Form::open(['route' => 'news.create','method' => 'POST','class' => 'w-100 col-12 flex-wrap' , 'files' => true])}}
            <div class="col-12 col-lg-6 px-0">
              <div class="form-group d-flex flex-wrap">
                <label class="col-12 col-sm-1 px-0 col-form-label">標題</label>
                <div class="col-12 col-sm-11 px-0 px-sm-3">
                  <input type="text" name="title" class="form-control" placeholder="標題" required>
                </div>
              </div>
              <div class="form-group d-flex flex-wrap">
                <label class="col-12 col-sm-1 px-0 col-form-label">圖片</label>
                <div class="col-12 col-sm-11 px-0 px-sm-3">
                  <div style="padding-top: 56.25%;" class="imgBox mb-3">
                    <div class="addIcon"><img src="/images/backstage/plus.svg" alt="">新增圖片</div>
                    <img id="preview_news_banner_image_1" style="width:100%;height:100%;position:absolute;top:0;" src="" alt="">
                    <input type="file" id="pet_avatar_upload_1" name="image[]" onchange="readURL(this);" onclick="clearInput(this)" targetid="preview_progressbarTW_img_1" required>
                    <input type="hidden" id="dataURL-1" name="dataURL">
                  </div>
                  {{-- <div class="text-right">
                    <button type="button" class="btn btn-edit">編輯</button>
                    <button type="button" class="btn btn-delete">刪除</button>
                  </div> --}}
                </div>
              </div>
            </div>
            <div class="col-12 col-lg-6 px-0">
              <div class="form-group d-flex flex-wrap">
                <label class="col-12 col-sm-1 px-0 col-form-label text-sm-center">內容</label>
                <div class="col-12 col-sm-11 px-0 px-sm-3">
                  <div class="form-group">
                    <textarea name="content" class="form-control" style="height: 307px" required></textarea>
                  </div>
                  <div class="d-flex flex-column">
                    <button type="submit" class="btn btn-dark mx-auto ml-lg-auto">新增</button>
                    <label class="text-center mx-auto ml-md-auto">
                      <small>請保持網路信號良好，以確保發送及接收正常</small>
                      <span class="d-block">*請勿重複點擊</span>
                    </label>
                  </div>
                </div>
              </div>
            </div>
            <hr class="w-100 border border-dark mt-0 mb-4">
          {{Form::close()}}
          
          @foreach($datas as $index => $data)
            <div class="col-12 col-sm-6 col-md-4 col-lg-3 px-1 d-flex flex-column mb-3">
              <div class="newsBox">
                {{Form::open(['route' => ['news.delete',$data->id],'method' => 'delete','class' => 'd-flex w-100 flex-wrap','onsubmit' => 'if(confirm("確定要刪除此則消息？")){return true}else{return false}'])}}
                <button class="delIcon" type="submit">
                  <i style="display: unset;" class="far fa-trash-alt"></i>
                </button>
                {{Form::close()}}
                <img src="{{$data->images[0]->image->url('small')}}" class="newsImg">
                <p class="h4">{{$data->title}}</p>
                <small>{{$data->created_at->format("m月d.Y")}}</small>
                <span class="text-justify mb-3 span-content">{{$data->content}}</span>
                <button name="edit_btn" type="button" class="btn btn-warning text-white" data-toggle="modal"
                  data-target="" data-id="{{$data->id}}">編輯</button>
              </div>
            </div>
          @endforeach

      <!-- 互動視窗 -->
      {{Form::open(['route' => 'news.update','class' => 'd-flex','files' => true , 'method' => 'PUT'])}}
      <input type="text" name="news_id" hidden>
      <div class="modal fade" id="newsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <!-- <h5 class="modal-title" id="exampleModalCenterTitle">Modal title</h5> -->
              <div class="form-group d-flex flex-wrap w-100 mb-0">
                <label class="col-12 col-sm-1 px-0 col-form-label">標題</label>
                <div class="col-12 col-sm-11 px-0 px-sm-3">
                  <input type="text" class="form-control" name="update_title" placeholder="標題" required>
                </div>
              </div>
              <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button> -->
            </div>
            <div class="modal-body">
              <div class="form-group d-flex flex-wrap w-100 mb-0">
                <label class="col-12 col-sm-1 px-0 col-form-label">圖片</label>
                <div class="col-12 col-sm-11 px-0 px-sm-3">
                  <div style="padding-top: 56.25%;" class="imgBox">
                    <div name="update_addIcon" class="addIcon"><img src="/images/backstage/plus.svg" alt="">新增圖片</div>
                    <img id="update_preview_news_banner_image_1" style="width:100%;height:100%;position:absolute;top:0;" src="" alt="">
                    <input type="file" id="update_pet_avatar_upload_1" name="image[]" onchange="readURL(this);" onclick="clearInput(this)" targetid="preview_progressbarTW_img_1">
                    <input type="hidden" id="update-dataURL-1" name="update_dataURL">
                  </div>
                  {{-- <div class="text-right">
                    <button type="button" class="btn btn-edit">編輯</button>
                    <button type="button" class="btn btn-delete">刪除</button>
                  </div> --}}
                </div>
              </div>
            </div>
            <div class="modal-body">
              <div class="form-group d-flex flex-wrap w-100 mb-0">
                <label class="col-12 col-sm-1 px-0 col-form-label text-left text-sm-center">內容</label>
                <div class="col-12 col-sm-11 px-0 px-sm-3">
                  <div class="form-group">
                    <textarea class="form-control" name="update_content" rows="6" required></textarea>
                  </div>
                  <div class="d-flex flex-column">
                    <button type="submit" class="btn btn-dark mx-auto ml-lg-auto">更新</button>
                    <label class="text-center mx-auto ml-md-auto">
                      <small>請保持網路信號良好，以確保發送及接收正常</small>
                      <span class="d-block">*請勿重複點擊</span>
                    </label>
                  </div>
                </div>
              </div>
            </div>
            <!-- <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary">Save changes</button>
            </div> -->
          </div>
        </div>      
      </div>
      {{Form::close()}}

      <!-- 截圖燈箱 -->
      <div class="modal fade" id="cutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true" data-backdrop="static">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-body">
              <div class="form-group d-flex flex-wrap w-100 mb-0">
                <div class="col-12">
                  <div class="cutBox mb-3">
                    <div class="preview-div" style="width: 100%; height:100%;" id="div_preview_progressbarTW_img_1">
                      <img id="preview_progressbarTW_img_1" src="">
                    </div>
                  </div>
                  <div class="text-right">
                    <img id="crop-rotate" src="/images/backstage/rotate90.png" class="crop-rotate" onclick="rotateImage()">
                    <button type="button" class="btn btn-secondary" onclick="cancelCrop()">取消</button>
                    <button type="button" class="btn btn-success" onclick="saveImage()">完成</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </main>
  </div>

  <script src="/js/jquery-3.3.1.min.js"></script>
  <script src="/js/bootstrap.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.4.1/cropper.js"></script>
  <script src="/js/cropping.js"></script>
  <script>
    $('button[name="add"]').on('click', function () {
      if ($(this).attr('value') == "關閉") {
        $('form').css('display', 'flex');
        $(this).css('background', '#ccc');
        $(this).attr('value', '新增');
        $(this).text('取 消');
      } else {
        $('form').css('display', 'none');
        $(this).removeClass('btn-secondary');
        $(this).css('background', '#dec09e');
        $(this).attr('value', '關閉');
        $(this).text('新 增');
      }
    });

    $("button[name='edit_btn']").on('click',function(){
      $.ajax({
            url: '/news/detail',
            type: 'GET',
            data:{
                id:$(this).data('id'),
            },
            error: function (xhr) {
                alert('發生錯誤');
            },
            success: function (response) {
                $("#update_pet_avatar_upload_1").val("");
                $("input[name='news_id']").val(response.id);
                $("input[name='update_title']").val(response.title);
                $("textarea[name='update_content']").text(response.content);
                $("#update_preview_news_banner_image_1").attr('src',response.img_url);
                $("div[name='update_addIcon']").css('opacity','0');
                $("#newsModal").modal('show');
            }
        });
    });
  </script>
</body>

</html>