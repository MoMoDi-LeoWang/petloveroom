<!-- 分頁 -->
@if ($paginator->hasPages())  <!-- 判斷有沒有分頁 -->
<div aria-label="Page navigation example">
    <ul class="pagination justify-content-center">
        @if (!$paginator->onFirstPage())  <!-- 判斷是否在第一頁 -->
        <li class="page-item">
            <a class="page-link" href="{{$paginator->previousPageUrl()}}" aria-label="Previous">
                <span aria-hidden="true">&laquo;</span>
                <span class="sr-only">Previous</span>
            </a>
        </li>
        @endif

        @if($paginator->currentPage() > 3)
            <li class="page-item"><a class="page-link" href="{{$paginator->url(1)}}">1</a></li>
        @endif

        @if($paginator->currentPage() > 4)
            .....
        @endif

        @foreach(range(1,$paginator->lastPage()) as $page)
            @if($page >= $paginator->currentPage() - 2 && $page <= $paginator->currentPage() + 2)
                <li class="page-item"><a class="page-link" style="background-color:{{$paginator->currentPage() == $page ? '#ddd' : ''}};" href="{{$paginator->url($page)}}">{{$page}}</a></li>
            @endif
        @endforeach

        @if($paginator->currentPage() < $paginator->lastPage() - 3)
            .....
        @endif

        @if($paginator->currentPage() < $paginator->lastPage() - 2)
            <li class="page-item"><a class="page-link" href="{{$paginator->url($paginator->lastPage())}}">{{$paginator->lastPage()}}</a></li>
        @endif

        @if($paginator->hasMorePages())  <!-- 判斷後續是否還有分頁 -->
        <li class="page-item">
            <a class="page-link" href="{{$paginator->nextPageUrl()}}" aria-label="Next">
                <span aria-hidden="true">&raquo;</span>
                <span class="sr-only">Next</span>
            </a>
        </li>
        @endif
    </ul>
</div>
@endif