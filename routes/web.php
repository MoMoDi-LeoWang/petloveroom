<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });



Route::get("/backstage/login","Auth\LoginController@showLoginForm");
Route::get("/backstage/logout" , "Auth\LoginController@logout");
Route::post("/petloveroom/back/login",['as' => 'back.login' , 'uses' => "Auth\LoginController@login"]);

Route::group(['namespace' => 'Web'] , function(){
    Route::get("/","HomeController@index");
    Route::get("/about","HomeController@about");
    Route::get("/service","HomeController@service");
    Route::get("/news","HomeController@news");
    Route::get("/contact","HomeController@contact");
    Route::get("/news/{id}/detail","HomeController@newsDetail");
    Route::get("/onepage","HomeController@onepage");
    Route::get("/loginStatus","HomeController@loginStatus");

    Route::post("/feedback/post",['as' => 'feedback.post' , 'uses' => 'HomeController@postFeedback']);

    Route::group(['middleware' => 'backstage.auth'] , function (){
        Route::get("/backstage/visitor-opinion",['as' => 'login.index' , 'uses' => "BackstageController@backstage" ]);
        Route::get("/visitor-opinion/detail", "BackstageController@getVisitorOpinionDetail");
        Route::delete("/visitor-opinion/detail/delete", ['as' => 'detail.delete' , 'uses' => "BackstageController@deleteVisitorOpinionDetail"]);

        Route::get("/backstage/news","BackstageController@newsBackstage");
        Route::get("/news/detail", "BackstageController@getNewsDetail");
        Route::post("/news/create",["as" => "news.create" , "uses" => "BackstageController@newsCreate"]);
        Route::put("/news/update",["as" => "news.update" , "uses" => "BackstageController@newsUpdate"]);
        Route::delete("/news/{id}/delete",["as" => "news.delete" , "uses" => "BackstageController@newsDelete"]);
    });
});
