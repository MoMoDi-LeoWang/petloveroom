<?php

namespace App\Models\News;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class News extends Model
{
    use SoftDeletes;

    protected $table = "news";
    protected $fillable = ['title' , 'content'];

    public function images(){
        return $this->hasMany(\App\Models\News\NewsImages::class,'news_id');
    }
}
