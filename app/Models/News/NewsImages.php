<?php

namespace App\Models\News;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Codesleeve\Stapler\ORM\EloquentTrait;
use Codesleeve\Stapler\ORM\StaplerableInterface;

class NewsImages extends Model implements StaplerableInterface
{
    use SoftDeletes,EloquentTrait;
    
    protected $table = "news_images";
    protected $fillable = ['news_id' , 'image'];

    public function __construct(array $attributes = array())
    {
        $this->hasAttachedFile('image', [
            'styles' => [
                'small'  => '256x144',
                'medium' => '512x288',
                'big'    => '768x432',
            ]
        ]);

        parent::__construct($attributes);
    }

    public function getAttributes()
    {
        return parent::getAttributes();
    }
}
