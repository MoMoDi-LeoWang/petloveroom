<?php

namespace App\Models\Home;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    protected $table = "feedback";
    protected $fillable = ['name' , 'email' , 'content'];
}
