<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Home\Feedback;
use Auth;
use App\Models\News\News;
use App\Models\News\NewsImages;

class HomeController extends Controller
{
    
    public function index(){
        return view("web.index");
    }

    public function about(){
        return view("web.about");
    }

    public function service(){
        return view("web.service");
    }

    public function contact(){
        return view("web.contact");
    }

    public function news(){
        $datas = News::with('images')
                     ->orderBy('created_at','desc')
                     ->take(8)
                     ->get();

        return view("web.news" , compact('datas'));
    }

    public function newsDetail($id){
        $news = News::find($id);
        if(empty($news)){
            return back();
        }

        $datas = News::where('id','!=',$id)
                     ->orderBy('created_at','desc')
                     ->take(4)
                     ->get();

        return view("web.news-detail", compact('news','datas'));
    }


    public function onepage(){
        return view("web.onepage");
    }

    public function login(){
        return view("web.login");
    }

    public function postFeedback(Request $request){
        if($request->get('name') == null || $request->get('email') == null || $request->get('content') == null){
            return redirect()->back()->with("success","發生錯誤，請重新操作。");
        }

        Feedback::create($request->all());

        return redirect()->back()->with("success","成功送出，我們將盡速回覆您。");
    }

    public function loginStatus(){
        dd(bcrypt(1111));
        dd(Auth::guest());
    }
}
