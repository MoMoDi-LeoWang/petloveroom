<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Home\Feedback;
use Auth;
use App\Models\News\News;
use App\Models\News\NewsImages;

class BackstageController extends Controller
{
    public function backstage(){
        $feedbacks = Feedback::orderBy('created_at','desc')->paginate(10);
        // dd($feedbacks);

        return view("web.backstage" , compact('feedbacks'));
    }

    public function getVisitorOpinionDetail(Request $request){
        $id = decrypt($request->get('id'));
        $detail = Feedback::find($id);
        $data = [
            'name'    => $detail->name,
            'email'   => $detail->email,
            'content' => $detail->content,
            'create_time' => $detail->created_at->format('Y/m/d H:i:s'),
            'is_read' => $detail->is_read,
        ];

        $detail->is_read = true;
        $detail->save();

        return response()->json($data);
    }

    public function deleteVisitorOpinionDetail(Request $request){
        foreach($request->get("opinion_chk") as $id){
            $feedback = Feedback::find($id);
            $feedback->delete();
        }

        return redirect()->back()->with('success','刪除成功');
    }

    public function newsBackstage(){
        $datas = News::with('images')
                     ->orderBy('created_at','desc')
                     ->take(8)
                     ->get();

        return view("web.news-backstage",compact('datas'));
    }

    public function newsCreate(Request $request){
        if($request->file("image") == null || $request->get("title") == null || $request->get("content") == null){
            return back();
        }

        $news_image = new NewsImages;
        $dataURL = $request->get("dataURL");
        foreach($request->file("image") as $index => $file){
            BackstageController::createImageFromDataURL($file,$dataURL);
            $news_image->image = $file;
            $news_image->save();
        }       

        $news = News::create($request->all());

        $news_image->news_id = $news->id;
        $news_image->save();

        return redirect()->back();
    }

    public function getNewsDetail(Request $request){
        $id = $request->get('id');
        $news = News::with('images')->find($id);
        $data = [
            'id'        => $news->id,
            'title'     => $news->title,
            'content'   => $news->content,
            'img_url'   => $news->images[0]->image->url('medium')
        ];

        return response()->json($data);
    }

    public function newsUpdate(Request $request){
        if($request->get("update_title") == null || $request->get("update_content") == null){
            return back();
        }

        $id = $request->get("news_id");
        $title = $request->get("update_title");
        $content = $request->get("update_content");
        $dataURL = $request->get("update_dataURL");

        $news = News::with('images')->find($id);
        if(empty($news)){
            return back();
        }

        $news_image = $news->images[0];

        if(!empty($dataURL)){
            foreach($request->file("image") as $index => $file){
                BackstageController::createImageFromDataURL($file,$dataURL);
                $news_image->image = $file;
                $news_image->save();
            }      
        }

        $news->title = $title;
        $news->content = $content;
        $news->save();

        return redirect()->back();
    }

    public function newsDelete($id,Request $request){
        $news = News::with('images')->find($id);
        
        if(empty($news)){
            return back();
        }

        $news_image = $news->images[0];
        $news_image->delete();

        $news->delete();

        return redirect()->back();
    }

    private function createImageFromDataURL($file, $dataURL){
        $resource = imagecreatefromjpeg($dataURL);
        imagejpeg($resource, $file->path());
        imagedestroy($resource);
    }

}
