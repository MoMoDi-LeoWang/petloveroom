var innH;

$(document).ready(function () {

  innH = $(window).innerHeight();

  $('.innH').css({
    'height': innH
  });

  $(window).resize(function () {

    innH = $(window).innerHeight();

    $('.innH').css({
      'height': innH
    });

  });

});

$(window).scroll(function () {
  if ($(window).scrollTop() > 0) {
    $('.navbar').css('background-color', 'rgba(10, 20, 146, 0.8)');
    $('.navbar, .nav-link').css('height', '60px');
    $('.line').css('bottom', '0');
    $('.logo').css('width', '50px');
    $('.logoTitle').css('display', 'inline-block');
  } else {
    $('.navbar, .nav-link').css('height', '150px');
    $('.line').css('bottom', '30%');
    $('.navbar').css('background-color', 'transparent');
    $('.logo').css('width', '100px');
  }
});

var slideout = new Slideout({
  'panel': document.getElementById('content'),
  'menu': document.getElementById('menu'),
  'padding': 300,
  'tolerance': 70,
  'touch': false
});

// Toggle button
document.querySelector('.fa-bars').addEventListener('click', function () {
  slideout.toggle();
});

slideout
  .on('beforeopen', function () {
    this.panel.classList.add('panel-open');
  })
  .on('open', function () {
    this.panel.addEventListener('click', close);
  })
  .on('beforeclose', function () {
    this.panel.classList.remove('panel-open');
    this.panel.removeEventListener('click', close);
  });

function close(eve) {
  eve.preventDefault();
  slideout.close();
}
