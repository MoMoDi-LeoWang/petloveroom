var step = 1;
var imageCropper = null;
var testInput ; 

Cropper.setDefaults({
    aspectRatio: 16 / 9,
    viewMode: 1,
    autoCropArea: 1,
    crop(event) {}
});

const readURL = (input) => {
    if (imageCropper != null){
        alert("請先完成前一張照片的裁切");
        input.value = null;
        return;
    }
    var num = input.id.substr(18);
    if (input.files && input.files[0]) {
        testInput = input;
        var imageTagID = input.getAttribute("targetID");

        var reader = new FileReader();

        reader.onload = function (e) {

            var img = document.getElementById(imageTagID);
            img.style.height = "500px";

            if (input.files[0]) {
                orientation(input.files[0], function (base64img, value) {
                    img.setAttribute("src", e.target.result)
                });
            }
        }
        reader.readAsDataURL(input.files[0]);

        show_crop(input);
        // closeUpload(input);

        if($("#is_change_" + num).length > 0){
            $("#is_change_" + num).attr('value','true');
        }
    }else{
        $('#preview_progressbarTW_img_' + num).attr('src', '');
        $('#preview_progressbarTW_img_' + num).css('opacity', '0');
        $('#dataURL-' + num).attr('value', '');
    }
}

const _arrayBufferToBase64 = (buffer) => {
    var binary = ''
    var bytes = new Uint8Array(buffer)
    var len = bytes.byteLength;
    for (var i = 0; i < len; i++) {
        binary += String.fromCharCode(bytes[i])
    }
    return window.btoa(binary);
}

const orientation = (file, callback) => {
    var fileReader = new FileReader();
    fileReader.onloadend = function () {
        var base64img = "data:" + file.type + ";base64," + _arrayBufferToBase64(fileReader.result);
        var scanner = new DataView(fileReader.result);
        var idx = 0;
        var value = 1; // Non-rotated is the default
        if (fileReader.result.length < 2 || scanner.getUint16(idx) != 0xFFD8) {
            // Not a JPEG
            if (callback) {
                callback(base64img, value);
            }
            return;
        }
        idx += 2;
        var maxBytes = scanner.byteLength;
        var littleEndian = false;
        while (idx < maxBytes - 2) {
            var uint16 = scanner.getUint16(idx, littleEndian);
            idx += 2;
            switch (uint16) {
                case 0xFFE1: // Start of EXIF
                    var endianNess = scanner.getUint16(idx + 8);
                    if (endianNess === 0x4949) {
                        littleEndian = true;
                    }
                    var exifLength = scanner.getUint16(idx, littleEndian);
                    maxBytes = exifLength - idx;
                    idx += 2;
                    break;
                case 0x0112: // Orientation tag
                    value = scanner.getUint16(idx + 6, littleEndian);
                    maxBytes = 0; // Stop scanning
                    break;
            }
        }
        if (callback) {
            callback(base64img, value);
        }
    }
    fileReader.readAsArrayBuffer(file);
}

const show_crop = (input) => {
    var num = input.id.substr(18);
    Cropper.setDefaults({
        minContainerHeight: 500,
        maxContainerHeight: 500,
        minContainerWidth: 736,
        maxContainerWidth: 736,
    });
    step = 1;
    imgOnload(input.id,$("#"+input.id).attr('targetID'));
}

const imgOnload = (id,imgID) => {
    var image = document.getElementById(imgID);
    $('#'+imgID).on('load', function (event) {
        if (step == 1) {
            if (imageCropper === null) {
                imageCropper = new Cropper(image);
                $('#cutModal').modal('show');
            }
        }
    });
}

const closeUpload = (input) => {
    $("#" + input.id).css('z-index','-1');
}

const rotateImage = () => {
    if (imageCropper != null) {
        imageCropper.rotate(90); //rotate
    }
}

const saveImage = () => {
    if (imageCropper != null) {
        var imageURL = imageCropper.getCroppedCanvas().toDataURL('image/jpeg');
        var num = imageCropper.element.id.substr(26);
        if ($("#newsModal").hasClass('show')){
            $("div[name='update_addIcon']").css('opacity', '0');
            $('#update_preview_news_banner_image_' + num).attr('src', imageURL);
            $('#update-dataURL-' + num).attr('value', imageURL);
        }else{
            $('.addIcon').css('opacity', '0');
            $('#preview_news_banner_image_' + num).attr('src', imageURL);
            $('#dataURL-' + num).attr('value', imageURL);
        }
        imageCropper.destroy();
        imageCropper = null;
        step = 2;
        $('#cutModal').modal('hide');
    }
}

function cancelCrop(){
    $('#cutModal').modal('hide');
    $('.addIcon').css('opacity', '1');
    testInput.value = null;
    imageCropper.destroy();
    imageCropper = null;
}

function clearInput(input){
    input.value = null;
    if (input.id.length < 25){
        var num = input.id.substr(18);
        $('.addIcon').css('opacity', '1');
        $('#preview_progressbarTW_img_' + num).attr('src', '');
        $("#preview_news_banner_image_" + num).attr('src', '');
        $('#dataURL-' + num).attr('value', '');
    }else{
        var num = input.id.substr(25);
        $("div[name='update_addIcon']").css('opacity', '1');
        $('#preview_progressbarTW_img_' + num).attr('src', '');
        $("#update_preview_news_banner_image_" + num).attr('src', '');
        $('#update-dataURL-' + num).attr('value', '');
    }
}

$("div[id='add_new_image']").on('click',function(){
    var num = $(".addroomImg").length;
    var item = '<div class="col-3 addroomImg image-new mb-5">'+
                    '<div id="camera_icon" class="cameraImg d-center">'+
                        '<input id="pet_avatar_upload_'+num+'" class="uploadInput" type="file" name="room_image[]" onchange="readURL(this);" onclick="clearInput(this)" targetid="preview_progressbarTW_img_'+num+'">'+
                        '<div class="preview-div" style="width: 180px;" id="div_preview_progressbarTW_img_'+num+'">'+
                            '<img id="preview_progressbarTW_img_'+num+'" src="">'+
                        '</div>'+
                        '<div id="div-crop-'+num+'" class="crop-hidden">'+
                            '<div class="d-flex justify-content-end" style="position:relative;cursor:pointer;z-index:1;">'+
                                '<img id="crop-rotate" src="/images/icon/rotate90.png" class="crop-rotate" onclick="rotateImage()">' +
                                '<img id="crop-finish" src="/images/backstage/ok.png" class="okIcon" onclick="saveImage()">' +
                            '</div>'+
                        '</div>' +
                        '<input id="dataURL-'+num+'" type="hidden" class="input" name="dataURL[]">'+
                    '</div>'+
               '</div>';
    $(this).before(item);
});

$("div[id='add_new_image_update']").on('click', function () {
    var num = $(".addroomImg-update").length;
    var item = '<div class="col-3 addroomImg-update image-update mb-5">' +
                    '<div id="camera_icon" class="cameraImg d-center">'+
                        '<input id="pet_avatar_upload_'+num+'" class="uploadInput" type="file" name="room_image[]" onchange="readURL(this);" onclick="clearInput(this)" targetid="preview_progressbarTW_img_'+num+'">'+
                        '<div class="preview-div" style="width: 180px;" id="div_preview_progressbarTW_img_'+num+'">'+
                            '<img id="preview_progressbarTW_img_'+num+'" src="">'+
                        '</div>'+
                        '<div id="div-crop-'+num+'" class="crop-hidden">'+
                            '<div class="d-flex justify-content-end" style="position:relative;cursor:pointer;z-index:1;">'+
                                '<img id="crop-rotate" src="/images/icon/rotate90.png" class="crop-rotate" onclick="rotateImage()">' +
                                '<img id="crop-finish" src="/images/backstage/ok.png" class="okIcon" onclick="saveImage()">' +
                            '</div>'+
                        '</div>' +
                        '<input id="dataURL-'+num+'" type="hidden" class="input" name="dataURL[]">'+
                    '</div>'+
               '</div>';
    $(this).before(item);
});