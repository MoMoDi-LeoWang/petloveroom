var wow = new WOW({
    boxClass: 'wow',
    animateClass: 'animated',
    offset: 0,
    mobile: true,
    live: true,
    callback: function (box) {
      // the callback is fired every time an animation is started
      // the argument that is passed in is the DOM node being
    },
    scrollContainer: null,
    resetAnimation: false,
  });
  wow.init();

  var innH;
  var logoH = $('.logo').height();
  var logoMF = Math.floor(logoH);

  $(document).ready(function () {

    if ($(window).scrollTop() > 50) {
      $('.navbar').css('background-color', 'rgba(10, 20, 146, 0.8)');
      $('.navbar, .nav-link').css('height', '50px');
      $('.line').css('bottom', '0');
      $('.logo').css('width', '50px');
      $('.logoTitle').css('display', 'inline-block');
    }

    innH = $(window).innerHeight();

    $('.innH').css({
      'height': innH
    });

    $(window).resize(function () {

      innH = $(window).innerHeight();

      $('.innH').css({
        'height': innH
      });

    });

  });

  $(window).scroll(function () {
    // console.log(150 - 0.1 * $(this).scrollTop());
    var i = Math.max(50, logoMF - 1 * $(this).scrollTop());
    var mass = Math.max(50, logoMF - 1 * $(this).scrollTop()) + 'px';

    $('.navbar, .nav-link').css({
      'height': mass
    });

    $('.logo').css({
      'width': mass
    });

    if (i > 50) {
      $('.navbar').css('box-shadow', 'unset');
      $('.logoTitle').css('display', 'none');
      $('nav').css('background-color', 'transparent');
      $('.navbar-brand').css('transform', 'translateY(10px)');
      $('.line').css('bottom', '30%');
    } else {
      $('.navbar').css('box-shadow', 'rgba(0, 0, 0, 0.1) 0px 1px 5px 0px');
      $('.logoTitle').css('display', 'inline-block');
      $('nav').css('background-color', 'rgba(10, 20, 146, 0.8)');
      $('.navbar-brand').css('transform', 'translateY(10px)');
      $('.line').css('bottom', '0');
    }
  });

  // 平滑
  $('.nav-link, .downBt, .footerItems').on('click', function (e) {
    if (this.hash !== "") {
      e.preventDefault();

      var hash = this.hash;

      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 600, function () {
        window.location.hash = hash;
      });
    }

  });

  $('body').scrollspy({
    target: '#navbar'
  });
