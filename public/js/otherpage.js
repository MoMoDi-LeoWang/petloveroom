$(document).ready(function () {
  if ($(window).scrollTop() == 0) {
    $('.navbar, .nav-link').css('height', '80px');
    $('.line').css('bottom', '20%');
    $('.navbar').css('background-color', '#0a1492');
    $('.logo').css('width', '70px');
    $('main').css('padding-top', '80px');
  }else{
    $('main').css('padding-top', '0');
  }
});

$(window).scroll(function () {
  if ($(window).scrollTop() > 100) {
    $('.navbar').css('background-color', 'rgba(10, 20, 146, 0.8)');
    $('.navbar, .nav-link').css('height', '60px');
    $('.line').css('bottom', '0');
    $('.logo').css('width', '50px');
    $('.logoTitle').css('display', 'inline-block');
    $('main').css('padding-top', '0');
  } else {
    $('.navbar, .nav-link').css('height', '80px');
    $('.line').css('bottom', '20%');
    $('.navbar').css('background-color', '#0a1492');
    $('.logo').css('width', '70px');
    $('main').css('padding-top', '80px');
  }
});

var wow = new WOW({
  boxClass: 'wow',
  animateClass: 'animated',
  offset: 0,
  mobile: true,
  live: true,
  callback: function (box) {
    // the callback is fired every time an animation is started
    // the argument that is passed in is the DOM node being
  },
  scrollContainer: null,
  resetAnimation: false,
});
wow.init();

var slideout = new Slideout({
  'panel': document.getElementById('content'),
  'menu': document.getElementById('menu'),
  'padding': 300,
  'tolerance': 70,
  'touch': false
});

// Toggle button
document.querySelector('.fa-bars').addEventListener('click', function () {
  slideout.toggle();
});

slideout
  .on('beforeopen', function () {
    this.panel.classList.add('panel-open');
  })
  .on('open', function () {
    this.panel.addEventListener('click', close);
  })
  .on('beforeclose', function () {
    this.panel.classList.remove('panel-open');
    this.panel.removeEventListener('click', close);
  });

function close(eve) {
  eve.preventDefault();
  slideout.close();
}